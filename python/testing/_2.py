#   Copyright (C) 2022 Stephen Fairchild (s-fairchild@users.sourceforge.net)
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program in the file entitled COPYING.
#   If not, see <http://www.gnu.org/licenses/>.

__all__ = ["main"]

_description_ = "Launch backend and make/break JACK port connections."

import ctypes
import os
import time
import random
import sys
from binascii import unhexlify
from datetime import datetime
from uuid import uuid4 as uuid

from idjc import FGlobs, PGlobs


def main():
    print(_description_)

    os.environ["has_head"] = "0"
    os.environ["extant_port_check"] = "0"

    backend = ctypes.CDLL(FGlobs.backend)
    read = ctypes.c_int()
    write = ctypes.c_int()
    if not backend.init_backend(ctypes.byref(read), ctypes.byref(write)):
        print("call to init_backend failed")
        return 5

    print(f"file descriptors for read/write are {read.value}, {write.value}")

    try:
        backend_write = os.fdopen(write.value, "w")
        backend_read = os.fdopen(read.value, "r")
    except OSError:
        "failed to open streams to backend"
        return 5

    print("awaiting reply")

    try:
        line = backend_read.readline()
    except IOError as e:
        print(e)
        return 5

    if line != "idjc backend ready\n":
        print(f"bad reply from backend: {line}")
        return 5

    print(f"backend replies with: {line}")

    def ports_list(filter_=""):
        backend_write.write(f"mx\nACTN=jackportread\nJFIL={filter_}\nJPRT=\nend\n")
        backend_write.flush()
        reply = backend_read.readline()

        if not reply.startswith("jackports="):
            raise ValueError(f"reply does not start with jackports= \"{reply}\"")

        return [str(unhexlify(x.lstrip("@-")), "ascii") for x in reply[10:-1].split()]

    hw = ports_list("hwoutputs"), ports_list("hwinputs")
    both = ports_list("outputs"), ports_list("inputs")
    sw = [list(set(b) - set(h)) for b, h in zip(both, hw)]

    print("Audio feedback noise warning!")
    print("Before doing this test physically turn down your audio or disconnect headphones/speakers.")
    print("User assumes responsibility for any hardware or hearing damage resulting from this test.")

    choice = input("Choose port class hardware/software/both: enter (h, s, b) ")
    choice = choice.strip()
    ports = {"h": hw, "s": sw, "b": both}.get(choice)

    if input("Add fake ports? (y/n) ").strip().lower().startswith("y"):
        for port in ports:
            for _ in range(2):
                port.append(f"{str(uuid())}:{str(uuid())}")

    legal_shit = input("Have you taken steps to protect your hearing? Enter Yes/No: ")
    if legal_shit != "Yes":
        print("guess not: exiting")
        sys.exit(0)

    def wire(operation, source, sink):
        backend_write.write(f"mx\nACTN=jack{operation}\n"
                            f"JPRT={source}\nJPT2={sink}\n"
                            f"end\n")
        backend_write.flush()

    start_time = datetime.now()

    while 1:
        repeat = 10
        while repeat:
            wire(random.choice(["connect", "disconnect"]),
                 random.choice(ports[0]), random.choice(ports[1]))
            repeat -= 1

        time.sleep(1)
        print(_description_)
        print(f"Subtest '{choice}', Uptime: {datetime.now() - start_time}")

    return 0
