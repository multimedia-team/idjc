"""Backend test stuff.

To run this code simply launch idjc with:
$ idjcdev=1 idjc run -T"""

#   Copyright (C) 2022 Stephen Fairchild (s-fairchild@users.sourceforge.net)
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program in the file entitled COPYING.
#   If not, see <http://www.gnu.org/licenses/>.

__all__ = ["main"]

import importlib
import os
import re


def main():
    options = {}

    print("\nTest Mode\n")
    ptn = re.compile("^(_(\S))\.py[a-z]?$")
    path = os.path.dirname(__file__)
    for each in sorted(os.listdir(path)):
        if match:= ptn.match(each):
            module = importlib.import_module('.' + match.group(1),
                                             package=__name__)
            if hasattr(module, "_description_"):
                options[match.group(2)] = module
                print(f"{match.group(2)}\t{module._description_}")

    try:
        option = options[input("\nPick an option: ").strip()]
    except KeyError:
        print("unsupported option -- quitting")
        return 5

    return option.main()
